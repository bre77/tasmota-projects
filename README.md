# Home Computer Integrated Lights Out (ILO) #

A ILO controller for Home Assistant, designed to replace Wake On Lan.

### GPIO Configuration ###

* RX Switch2
* TX Relay2i
* D4 Relay1i
* D3 Switch1
* D7 Led1i

### Wiring ###

* Motherboard LED- to **RX**
* Cable LED- to **TX**
* Motherboard LED+ to Cable LED+
* Motherboard SWITCH+ to **D4**
* Cable SWITCH- to Motherboard SWITCH-
* Cable SWITCH+ to **D3**
* Nothing to **D7**
* Motherboard USB 5V to **5V**
* Motherboard USB GND to **GND**

### Console Setup ###

`SetOption19 1`

`switchMode1 2`

`switchMode2 2`

`rule1 ON Switch2#State DO backlog var1 %value%; Power2 %value% ENDON`

`rule1 1`

`rule2 ON Power2#State!=%var1% Do backlog Power1 1; Power1 0 ENDON`

`rule2 1`